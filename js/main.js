//Author: Jeroen Rijsdijk
window.onload = () => {
  const searchbar = document.getElementById("js--searchbar");
  const cardsList = document.getElementsByClassName("card");
  const cardHeaders = document.getElementsByClassName("js--cardHeader");
  const overlay = document.getElementById("js--overlay");
  const colors = ["#0074D9","#FF4136"];
  const randomNumber = Math.floor(Math.random() * 2);

  for(let i = 0; i < cardHeaders.length; i++){
    cardHeaders[i].style.backgroundColor = colors[randomNumber];
  }

  searchbar.onkeyup = (event) => {
    let filter = event.target.value.toUpperCase();
    for (let i = 0; i < cardsList.length; i++){
      let innerHTML = cardsList[i].innerHTML.toUpperCase();
      if(innerHTML.indexOf(filter) !== -1){
        cardsList[i].style.display = "";
      }
      else{
        cardsList[i].style.display = "none";
      }
    }
  }
}
